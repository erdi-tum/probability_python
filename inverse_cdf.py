#Inverse CDF
import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt

plt.figure(0)

# Generate data from the normal distribution
data = norm.rvs(10.0, 1, size=5000)

count, bins, ignored = plt.hist(data, 100, density=True, alpha=0.6,color='red')


# Parameter estimates for generic data.In other words, Find the parameters such as mean and variance from the given data.
# We assume that the data has normal distribution
mu, std = norm.fit(data)
print("Mean of the given data: " , mu)
print("Standard devitation of the given data: " , std)


# Plot the PDF.
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2)
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)




from scipy.stats import expon
plt.figure(1)
# Sample data from the exponential distribution with lambda = 1
data_sample_from_exp = np.random.exponential(1,size=5000)

count, bins, ignored = plt.hist(data_sample_from_exp, 100, density=True, alpha=0.6,color='blue')

xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = expon.pdf(x,loc=0,scale=1)

plt.plot(x, p, 'k', linewidth=2)

title = "Exponential distribution $\lambda = 1 $ "

plt.title(title)

plt.show()
